//
//  LynnRefreshState.swift
//  SwiftUIPullToRefresh
//
//  Created by apple on 2021/7/15.
//

import SwiftUI
@available(iOS 13.0, *)
public struct Item {
    let bounds: Anchor<CGRect>
}
// MARK: - Preferences
@available(iOS 13.0, *)
public struct HeaderBoundsPreferenceKey: PreferenceKey {
    
    public static var defaultValue: [Item] = []
    
    // 每次有新的init(bounds)就加入value数组
    public static func reduce(value: inout [Item], nextValue: () -> [Item]) {
        value.append(contentsOf: nextValue())
    }
}
@available(iOS 13.0, *)
public struct FooterBoundsPreferenceKey: PreferenceKey {

    public static var defaultValue: [Item] = []
    
    public static func reduce(value: inout [Item], nextValue: () -> [Item]) {
        value.append(contentsOf: nextValue())
    }
}

// MARK: - Environment
@available(iOS 13.0, *)
public struct HeaderRefreshDataKey: EnvironmentKey {
    public static var defaultValue: RefreshData = .init()
}
@available(iOS 13.0, *)
public struct FooterRefreshDataKey: EnvironmentKey {
    public static var defaultValue: RefreshData = .init()
}
@available(iOS 13.0, *)
public extension EnvironmentValues {
    var headerRefreshData: RefreshData {
        get { self[HeaderRefreshDataKey.self] }
        set { self[HeaderRefreshDataKey.self] = newValue }
    }
    
    var footerRefreshData: RefreshData {
        get { self[FooterRefreshDataKey.self] }
        set { self[FooterRefreshDataKey.self] = newValue }
    }
}

// MARK: - Refresh State Data

public enum RefreshState: Int {
    case invalid // 无效
    case stopped // 停止
    case triggered // 触发
    case loading // 加载
}

public struct RefreshData {
    var thresold: CGFloat = 0
    var progress: Double = 0
    var refreshState: RefreshState = .invalid
}
